console.log("Hello World");

// [Section] Functions
/*
	functions in javascript are lines/blocks of codes that tell our device to perform a certain task when they are called/invoked

	functions are mostly created to create complicated tasks to run several lines of code in succession
*/

// [Section] Function Declaration
// defines a function with the specified parameters.
/*
Syntax:
	function functionName(){
		codes to be executed (statements);
	}
*/
function printName(){
	console.log("Hello Again");
}

printName();

// [Section] Function Invocation
// the code block and statements inside the function is not immediately executed when the function is defined.
// the codes will be executed when the function is invoked or called

// let's reuse the codes inside the printName function
printName();

// declaredFunction(); we cannot invoke a function that is not yet defined

// [Section] Function declaration vs expressions

// Function Declaration
// a function can be created through function declaration through the use of "function" keyword and adding a function name.

// in JS, functions can be hoisted. hoisting is the JS manner wherein functions can be run or used before they are created
declaredFunction();

function declaredFunction(){
	console.log("Hello from declaredFunction()");
}

declaredFunction();

/*
let and const variables cannot be hoisted in JS
console.log(name);

let name;
*/

// Function Expression
// a function an be stored in a variable.
// a function expression is an anonymous function assigned to a variable.
/*
Syntax
	let/const variableFunctionName = function(){
		codes to be executed/statments
	}
*/
// variableFunction();
/*
	error - function expressions, being let/const variables that have function values, cannot be hoisted in JS
*/

let variableFunction = function(){
	console.log("Hello from variableFunction()");
}

variableFunction();


let funcExpression = function funcName(){
	console.log("Hello from the other side!");
}

// funcName();
funcExpression();

// reassigning declared functions and function expression to new anonymous functions

declaredFunction = function(){
	console.log("updated declaredFunction");
}

declaredFunction();

funcExpression = function(){
	console.log('updated funcExpression()')
}

funcExpression();
// we cannot reassign a new function expression initialized with const keyword
const constFunction = function(){
	console.log("Const Function");
}
constFunction();

/*constFunction = function(){
	console.log("cannot be reassigned!");
}
constFunction();*/

// [Section] Function Scoping
/*
	scope is the accessibility (visibility) of variables

	JS has 3 scopes for its variables
		1. local/block scope
		2. global scope
		3. function scope
			- Javascript has function scope: each function creates a new scope
			- variables defined inside a function are not accessible from outside the function
*/

let globalVar = "Jane Doe";

{
	let localVar = "John Doe";
	console.log(localVar);
	console.log(globalVar); //accessible since  the globalVar is a global variable 
}

console.log(globalVar);
// console.log(localVar); - returns error because the localVar can only be accessed inside the curly brace where it is created

function showNames(){
	const functionConst = "Joe";
	let functionLet = "John";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// returns an error because they are inside the function showNames()
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions
function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedVar = "John";
		console.log(name); // will work because nestedFunction() is still inside the function (myNewFunction) where "name" variable is declared
	}
	// console.log(nestedVar); results to an error because of function scoping in JS
	nestedFunction(); // works because nestedFunction is called inside the function where it is declared
}
myNewFunction();
// nestedFunction(); doesnt work because nestedFunction is called outside the function where it is declared

let globalName = "Alexandro";
function newFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
}
newFunction2();
// console.log(nameInside); results to an error

// [Section] use of alert()
// alert() allows us to show a small window at the top of our browser page to show information to our users.
// alert("Hello World!");
// we can also use alert() to show a message to the users from a later function invocation
function sampleAlert(){
	alert("Hello, User!");
}
// sampleAlert();

console.log('I will only log in the console after the alert is closed');

// [Section] use of prompt()
// prompt() allows us to show a small window at the top of the browser page to gather user input. much like alert, it will have the page wait until the user completes or enters their input.
// input from the prompt()  will be returned as a String data type once the user dismisses the window

/*
prompt returns empty string if the user clicks 'ok' button without entering any input and null if the user cancels the prompt()
*/
let samplePrompt = prompt("Enter your name:");
console.log("Hello, " + samplePrompt);
console.log(typeof samplePrompt) //returns a string data type

/*
Miniactivity
	- create a "printWelcomeMessage()" function that has the following specifications:
		- create 2 variables that come from prompt():
			- firstName
			- lastName

		- log in the console the message "Hello firstName lastName! Welcome to my page!"

	- invoke the function

	5 minutes 10:25; kindly send a screenshot of your output in our Google chat, if you can
*/

function printWelcomeMessage(){
	let firstName = prompt("Enter first name");
	let lastName = prompt("Enter last name");

	console.log("Hello " + firstName + " " + lastName + "! Welcome to my page!");
}
printWelcomeMessage();
// console.log(firstName);

// [Section] Function Naming Conventions
	
	// function names should be definitive of the task it will perform. it usually starts with a verb
	
function getCourses(){
	let courses = [ "Science 101", "Math 101", "English 101" ];

	console.log(courses);
}
getCourses();

	// avoid generic names to avoid confusion within our codes
function get(){
	let name = "Jamie";
	console.log(name);
}
get();

	// avoid pointless and inappropriate function names
function foo(){
	console.log(25%5);
}
foo();

	// name our functions in small caps. follow camelCasing when naming variables and functions with more than 2 words
function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1 500 000");
}
displayCarInfo();

